//
//  Pol_VallsApp.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 3/4/21.
//

import SwiftUI

@main
struct Pol_VallsApp: App {
    
    //Instantiate DB controller and add to environment
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
