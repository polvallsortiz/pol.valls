//
//  ContentView.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 3/4/21.
//

import SwiftUI

struct ContentView: View {
    
    //Store shared by multiple views
    @Environment(\.managedObjectContext) private var viewContext

    var body: some View {
        @environmentObject let groupsStore = GroupsStore()

        GroupsView(groupsStore: groupsStore, groupsPresenter: GroupsPresenter(groupsService: GroupsService(), groupsPresenterDelegate: groupsStore))
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
