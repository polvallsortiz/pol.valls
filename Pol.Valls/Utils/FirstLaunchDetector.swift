//
//  FirstLaunchDetector.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 7/4/21.
//

import Foundation

class FirstLaunchDetector {
    static func isAppAlreadyLaunchedOnce() -> Bool {
        let defaults = UserDefaults.standard
        if let _ = defaults.string(forKey: "isAppAlreadyLaunchedOnce") {
            return true
        } else {
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            return false
        }
    }
}
