//
//  GroupDetailView.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 6/4/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct GroupDetailView: View {
    
    static let groupDateFormatter: DateFormatter = {
       let formatter = DateFormatter()
       formatter.dateFormat = "dd/MM/YYYY"
       return formatter
   }()
    
    var i : Int
    @Binding var groups: [Group]
    @State var favourite: Bool
    @State var images = false
    
    
    var body: some View {
        ScrollView {
            ZStack{
                AnimatedImage(url: URL(string: groups[i].imageUrl!))
                    .resizable()
                    .placeholder(UIImage(named: "placeholder"))
                    .onTapGesture {
                        images.toggle()
                    }
                VStack {
                    HStack {
                        Text(groups[i].name!)
                            .bold()
                            .foregroundColor(Color.black)
                            .background(Color.white.opacity(0.8))
                        Spacer()
                        Text("\(groups[i].date!, formatter: Self.groupDateFormatter)")
                            .foregroundColor(Color.black)
                            .background(Color.white.opacity(0.8))
                            .font(.system(size: 10))
                    }
                    .padding(.all, 20)
                    Spacer()
                    HStack {
                        Text(groups[i].shortDesc!)
                            .foregroundColor(Color.black)
                            .background(Color.white.opacity(0.8))
                        Spacer()
                    }
                    .padding(.all, 20)
                }
            }
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 200, maxHeight: 200, alignment: .topLeading)
            HStack {
                Spacer()
                Button(action: {
                    groups[i].favourite = !groups[i].favourite
                    favourite.toggle()
                    PersistenceController.shared.save()
                })
                {
                    if(favourite) {
                        Image("favoriteBlack")
                    }
                    else {
                        Image("favoriteBorder")
                    }
                }
                .padding(.all, 20)
            }
            Text (groups[i].desc!)
                .padding(10)
            .navigationTitle(groups[i].name!)
            .onAppear(perform: { favourite = groups[i].favourite })
            .fullScreenCover(isPresented: $images) { () -> AnyView in
                return AnyView(GroupDetailCarrousel(groupId: Int(groups[i].id), images: [String]()))
            }
        }
    }
}
