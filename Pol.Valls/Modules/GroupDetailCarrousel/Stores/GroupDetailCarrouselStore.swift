//
//  GroupDetailCarrouselStore.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 7/4/21.
//

import Foundation

class GroupDetailCarrouselStore: ObservableObject {
    enum State {
        case loading
        case error
        case loaded(images: [String])
    }
    
    @Published var state: State = .loading
}

extension GroupDetailCarrouselStore {
    
    //Functions to UI
    
    func showError() {
        self.state = .error
    }
    
    func showImages(images: [String]) {
        self.state = .loaded(images: images)
    }
    
}
