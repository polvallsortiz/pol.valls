//
//  GroupDetailCarrouselService.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 7/4/21.
//

import SwiftyJSON
import Alamofire

class GroupDetailCarrouselService {
    
    //API Functions
    
    func apiGroupDetailImagesRequest(groupId: Int, completionHandler: @escaping([String], Bool) -> Void) {
        var images = [String]()
        let headers : HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept":"application/json"
        ]
        AF.request("https://practica-slashmobility.firebaseio.com/images/\(groupId).json", method: .get, headers: headers).validate().responseJSON { response in
            if let result = response.data {
                if let json = try? JSON(data: result) {
                    for(_, object) in json {
                        images.append(object.string ?? "")
                    }
                    completionHandler(images,true)
                }
                else {
                    completionHandler(images, false)
                }
            }
            else {
                completionHandler(images, false)
            }
        }
    }
}
