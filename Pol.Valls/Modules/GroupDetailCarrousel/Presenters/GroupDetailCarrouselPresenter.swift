//
//  GroupDetailCarrouselPresenter.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 7/4/21.
//

import Foundation

protocol GroupDetailCarrouselPresenterProtocol: AnyObject {
    func getImages(groupId: Int, completionHandler: @escaping ([String]) -> Void)
}

class GroupDetailCarrouselPresenter: GroupDetailCarrouselPresenterProtocol {
    
    private let groupDetailCarrouselService: GroupDetailCarrouselService
    
    init(groupDetailCarrouselService: GroupDetailCarrouselService) {
        self.groupDetailCarrouselService = groupDetailCarrouselService
    }
    
    //Functions from Store
    
    func getImages(groupId: Int, completionHandler: @escaping ([String]) -> Void)  {
        groupDetailCarrouselService.apiGroupDetailImagesRequest(groupId: groupId, completionHandler: { (images, completed) in
            if(completed) {
                completionHandler(images)
            }
            else {
               completionHandler([String]())
            }
        })
    }
    
    
}
