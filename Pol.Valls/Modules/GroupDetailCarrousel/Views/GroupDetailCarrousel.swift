//
//  GroupDetailCarrousel.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 7/4/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct GroupDetailCarrousel: View {

    enum StateOpts {
        case loading
        case error
        case loaded(images: [String])
    }
    
    var groupId : Int
    @State var images : [String]
    
    @Environment(\.presentationMode) var presentationMode
    @State var state : StateOpts = .loading
    @State var totalCanvasWidth : Float = 0

    
    @ViewBuilder
    var body: some View {
        switch state {
        case .loading:
            AnyView(HStack(spacing:10) {
                ProgressView()
                Text("Descargando imágenes...")
            })
            .onAppear(perform: {
                let presenter = GroupDetailCarrouselPresenter(groupDetailCarrouselService: GroupDetailCarrouselService())
                presenter.getImages(groupId: groupId, completionHandler: {(images) in
                    if(images.isEmpty) {
                        state = .error
                    }
                    else {
                        state = .loaded(images: images)
                    }
                })
            })
        case .error:
            AnyView(VStack {
                HStack {
                    Spacer()
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    })
                    {
                        Image("closeWhite")
                    }
                }
                Spacer()
                Text("Error descargando imagenes del grupo")
                    .foregroundColor(Color.white)
                Spacer()
            }.background(Color.black))
        case .loaded(let images):
            AnyView(VStack {
                HStack {
                    Spacer()
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    })
                    {
                        Image("closeWhite")
                    }
                }
                .padding(.all, 20)
                GeometryReader { geometry in
                    ImageCarouselView(numberOfImages: images.count) {
                        ForEach(images, id: \.self) { imageUrl in
                            AnimatedImage(url: URL(string: imageUrl))
                                .resizable()
                                .scaledToFit()
                                .frame(width: geometry.size.width, height: geometry.size.height)
                                .clipped()
                        }
                    }
                }.frame(alignment: .center)
            }
            .background(Color.black)
            )
        }
    }
}
