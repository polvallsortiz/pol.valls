//
//  GroupsStore.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 5/4/21.
//

import Foundation

class GroupsStore: ObservableObject {
    enum State {
        case loading
        case error(message: String)
        case loaded
        case empty
    }
    
    @Published var state: State = .loading
    @Published var groups: [Group] = [Group]()
}

extension GroupsStore: GroupsPresenterDelegate {
    
    //Functions to UI
    
    func showFetching() {
        self.state = .loading
    }
    
    //Functions from Presenter
    
    func fetchGroupsResult(error: Bool) {
        if(error) {
            self.state = .error(message: "Error al descargar los grupos")
        }
    }
        
    func setGroups(groups: [Group]) {
        if(!groups.isEmpty) {
            self.groups = groups
            self.state = .loaded
        }
        else {
            self.state = .empty
        }
    }
    
    func getFavoriteGroups() -> [Group] {
        var result = [Group]()
        for group : Group in self.groups {
            if(group.favourite) {
                result.append(group)
            }
        }
        return result
    }

}
