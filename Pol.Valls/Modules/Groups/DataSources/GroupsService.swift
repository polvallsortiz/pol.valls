//
//  GrupoNetwork.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 4/4/21.
//

import SwiftyJSON
import Alamofire

class GroupsService {
    
    //API Functions
    
    func apiGroupRequest(completionHandler: @escaping (Bool) -> Void) {
        let persistenceController = PersistenceController.shared
        
        persistenceController.flushGroups { (completed) in
            if(completed) {
                let headers : HTTPHeaders = [
                    "Content-Type":"application/json",
                    "Accept":"application/json"
                ]
                AF.request("https://practica-slashmobility.firebaseio.com/groups.json", method: .get, headers: headers).validate().responseJSON { response in
                    if let result = response.data {
                        if let json = try? JSON(data: result) {
                            for(_, object) in json {
                                persistenceController.createGroup(json: object, completionHandler: {(desc, error) in
                                    if(error) {
                                        completionHandler(false)
                                        return
                                    }
                                })
                            }
                            completionHandler(true)
                        }
                    }
                    else {
                        completionHandler(false)
                    }
                }
            }
            else {
                completionHandler(false)
            }
        }
    }
    
    //DB Functions
    
    func getAllGroups(completionHandler: @escaping ([Group]) -> Void) {
        let persistenceController = PersistenceController.shared
        persistenceController.getGroups { (groups, error) in
            completionHandler(groups)
        }
    }
    
}
