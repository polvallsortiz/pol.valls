//
//  GroupsPresenter.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 5/4/21.
//

import Foundation

protocol GroupsPresenterProtocol: AnyObject {
    func fetchGroups()
    func getGroups()
}

protocol GroupsPresenterDelegate: AnyObject {
    func showFetching()
    func setGroups(groups: [Group])
    func fetchGroupsResult(error: Bool)
}

class GroupsPresenter: GroupsPresenterProtocol {
    
    private let groupsService : GroupsService
    weak private var groupsPresenterDelegate: GroupsPresenterDelegate?
    
    private var groups : [Group]
    
    init(groupsService: GroupsService, groupsPresenterDelegate: GroupsPresenterDelegate?) {
        self.groupsService = groupsService
        self.groupsPresenterDelegate = groupsPresenterDelegate
        groups = [Group]()
    }
    
    func setViewDelegate(groupsPresenterDelegate: GroupsPresenterDelegate?) {
        self.groupsPresenterDelegate = groupsPresenterDelegate
    }
    
    //Functions from viewController in SwiftUI
    
    func fetchGroups() {
        groupsPresenterDelegate?.showFetching()
        groupsService.apiGroupRequest(completionHandler: { (completed) in
            if(completed) {
                self.getGroups()
            }
            else {
                self.groupsPresenterDelegate?.fetchGroupsResult(error: !completed)
            }
        })
    }
    
    func getGroups() {
        if(FirstLaunchDetector.isAppAlreadyLaunchedOnce()) {
            groupsService.getAllGroups { (groups) in
                self.groupsPresenterDelegate?.setGroups(groups: groups)
            }
        }
        else {
            self.fetchGroups()
        }
    }
    
}
