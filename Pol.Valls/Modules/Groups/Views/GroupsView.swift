//
//  GroupsView.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 5/4/21.
//

import SwiftUI

struct GroupsView: View {
    var groupsPresenter : GroupsPresenterProtocol
    @ObservedObject var groupsStore : GroupsStore
    
    
    init(groupsStore: GroupsStore, groupsPresenter: GroupsPresenterProtocol) {
        self.groupsStore = groupsStore
        self.groupsPresenter = groupsPresenter
    }
    
    func getAnyView() -> AnyView {
        var anyView = AnyView(_fromValue: ())
        switch groupsStore.state {
        case .loading:
            anyView = AnyView(LoadingGroupsView())
        case .error(let message):
            anyView = AnyView(
                Text("No hay grupos")
                    .alert(isPresented: .constant(true)){
                        Alert(title: Text(message), message: Text(""), dismissButton: .default(Text("Aceptar")))
                    })
        case .loaded:
            anyView = AnyView(GroupsListView(groups: $groupsStore.groups, groupsList: [Group]()))
        case .empty:
            anyView = AnyView(Text("No hay grupos"))
        }
        return anyView!
    }
    
    var body: some View {
        NavigationView { () -> AnyView in
            return AnyView(
            VStack {
                getAnyView()
                .navigationTitle("Practica")
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button(action: {
                            groupsPresenter.fetchGroups()
                        })
                        {
                            Image("refreshIcon")
                        }
                    }
                    ToolbarItem(placement: .navigationBarTrailing) {
                        NavigationLink(destination: FavoriteGroupsListView(groups: $groupsStore.groups, favoritesGroups: [Group]())){
                            Button(action: {})
                            {
                                Image("favoriteBlack")
                            }
                        }
                    }
                }
            })
        }
        .onAppear(perform: groupsPresenter.getGroups)
    }
}
