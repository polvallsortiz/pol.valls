//
//  FavoriteGroupsListView.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 6/4/21.
//

import SwiftUI

class FavoriteGroupsUtils {
   
    static func favoritesArray(groups: [Group]) -> [Group] {
        var result = [Group]()
        for group : Group in groups {
            if(group.favourite) {
                result.append(group)
            }
        }
        return result
    }
    
    static func getIndex(groups: [Group], group: Group) -> Int {
        return groups.firstIndex{$0.name == group.name} ?? 0
    }
}

struct FavoriteGroupsListView: View {
    
    @Binding var groups: [Group]
    @State var favoritesGroups: [Group]
    
    var body: some View {
        VStack {
            if(favoritesGroups.isEmpty ) {
                Text("No hay grupos")
            }
            else {
                let withIndex = favoritesGroups.enumerated().map({ $0 })
                List(withIndex, id: \.element.id) { index, group in
                    ZStack{
                        NavigationLink(destination: GroupDetailView(i: FavoriteGroupsUtils.getIndex(groups: groups, group: group) , groups: $groups, favourite: groups[FavoriteGroupsUtils.getIndex(groups: groups, group: group)].favourite)) {
                            EmptyView()
                        }
                        .hidden()
                        GroupsRowView(group: group)
                    }
                }
                .listStyle(PlainListStyle())
            }
        }
        .navigationTitle("Favoritos")
        .onAppear(perform: {
            favoritesGroups = FavoriteGroupsUtils.favoritesArray(groups: groups)
        })
    }
}
