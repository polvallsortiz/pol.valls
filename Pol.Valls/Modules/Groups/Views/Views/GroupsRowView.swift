//
//  GroupsRow.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 6/4/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct GroupsRowView: View {
    
    static let groupDateFormatter: DateFormatter = {
       let formatter = DateFormatter()
       formatter.dateFormat = "dd/MM/yyyy"
       return formatter
   }()
    
    var group : Group
    
    
    var body: some View {
        VStack {
            HStack {
                Text(group.name!)
                    .bold()
                    .foregroundColor(Color.black)
                    .background(Color.white.opacity(0.8))
                Spacer()
                Text("\(group.date!, formatter: Self.groupDateFormatter)")
                    .foregroundColor(Color.black)
                    .background(Color.white.opacity(0.8))
                    .font(.system(size: 10))
            }
            .padding(.all, 20)
            Spacer()
            HStack {
                Text(group.shortDesc!)
                    .foregroundColor(Color.black)
                    .background(Color.white.opacity(0.8))
                Spacer()
            }
            .padding(.all, 20)
        }
        .background(
            AnimatedImage(url: URL(string: group.imageUrl!))
                .placeholder(UIImage(named: "placeholder"))
                .transition(.fade(duration: 0.5))
                .resizable()
            .edgesIgnoringSafeArea(.all)
        )
        .frame(minHeight: 200, maxHeight: 200)
    }
}

struct GroupsRowView_Previews: PreviewProvider {
    static var previews: some View {
        GroupsRowView(group: PersistenceController.shared.getPreviewGroup())
            .previewLayout(.fixed(width: 900, height: 300))
    }
}
