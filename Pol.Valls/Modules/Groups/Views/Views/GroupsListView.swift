//
//  GroupsListView.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 6/4/21.
//

import SwiftUI

struct GroupsListView: View {
    
    @Binding var groups: [Group]
    @State var groupsList : [Group]

    var body: some View {
        let withIndex = groupsList.enumerated().map({ $0 })
        List(withIndex, id: \.element.id) { index, group in
            ZStack{
                NavigationLink(destination: GroupDetailView(i: index , groups: $groups, favourite: groups[index].favourite)) {
                    EmptyView()
                }
                .hidden()
                GroupsRowView(group: group)
            }
        }
        .listStyle(PlainListStyle())
        .onAppear(perform: {
            groupsList = groups
        })
    }
}
