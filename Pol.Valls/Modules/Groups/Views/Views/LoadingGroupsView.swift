//
//  LoadingGroupsView.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 6/4/21.
//

import SwiftUI

struct LoadingGroupsView: View {
    
    var body: some View {
        HStack(spacing: 10) {
            ProgressView()
            Text("Descargando grupos...")
        }
    }
}

struct LoadingGroupsView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingGroupsView()
    }
}
