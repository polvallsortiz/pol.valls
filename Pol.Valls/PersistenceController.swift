//
//  PersistenceController.swift
//  Pol.Valls
//
//  Created by Pol Valls Ortiz on 5/4/21.
//

import Foundation
import CoreData
import SwiftyJSON

struct PersistenceController {
    
    static let shared = PersistenceController()
    
    //Storage for Core Data
    let container: NSPersistentContainer
    
    static var preview: PersistenceController = {
            let controller = PersistenceController(inMemory: true)

            // Create 10 example programming languages.
            for i in 0..<10 {
                let group = Group(context: controller.container.viewContext)
                group.id = Int16(i)
                group.name = "Example Group \(i)"
                group.desc = "Description Group \(i)"
                group.shortDesc = "ShortDesc \(i)"
                group.imageUrl = "https://images.unsplash.com/photo-1616577441006-8a6b3fc50a7d?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=634&q=80"
                group.date = Date()
            }

            return controller
        }()
    
    // An initializer to load Core Data, optionally able
       // to use an in-memory store.
        init(inMemory: Bool = false) {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) // get the Documents folder path
            let pathForDocumentDir = documentsPath[0]
            print("pathForDocumentDir: \(pathForDocumentDir)")
           // If you didn't name your model Main you'll need
           // to change this name below.
           container = NSPersistentContainer(name: "Pol.Valls")

           if inMemory {
               container.persistentStoreDescriptions.first?.url = URL(fileURLWithPath: "/dev/null")
           }

           container.loadPersistentStores { description, error in
               if let error = error {
                   fatalError("Error: \(error.localizedDescription)")
               }
           }
       }
    
    func save() {
        let context = container.viewContext

        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Show some error here
            }
        }
    }
    
    
    //Function for SwiftUI Previewer
    func getPreviewGroup() -> Group {
        let group = Group(context: PersistenceController.shared.container.viewContext)
        group.id = Int16(1)
        group.name = "Deportes"
        group.desc = "Description Group 1"
        group.shortDesc = "ShortDesc 1"
        group.imageUrl = "https://firebasestorage.googleapis.com/v0/b/practica-slashmobility.appspot.com/o/groups%2F4.jpg?alt=media&token=841e0cc8-2407-47da-a95f-56b633682566"
        group.date = Date()
        return group
    }

    //Deletes all the groups from DB
    func flushGroups(completionHandler: @escaping(Bool) -> Void) {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Group")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try container.viewContext.execute(deleteRequest)
            completionHandler(true)
        } catch let error as NSError {
            print(error)
            completionHandler(false)
        }
    }
    
    //Creates a group in the DB given a Group JSON
    func createGroup(json: JSON, completionHandler: @escaping(String, Bool) -> Void) {
        let context = container.viewContext
        let group = Group(context: context)
        if(json["id"].uInt16 != nil) {
            group.id = Int16(json["id"].uInt16!)
            group.name = json["name"].string != nil ? json["name"].string! : ""
            group.desc = json["description"].string != nil ? json["description"].string! : ""
            group.shortDesc = json["descriptionShort"].string != nil ? json["descriptionShort"].string! : ""
            group.imageUrl = json["defaultImageUrl"].string != nil ? json["defaultImageUrl"].string! : ""
            group.date = json["date"].double != nil ? Date(timeIntervalSince1970: (json["date"].double! / 1000.0)) : Date()
            do {
                try context.save()
                completionHandler("", false)
            }
            catch {
                completionHandler("Error saving group \(group.id)", true)
            }
        }
        else {
            completionHandler("ID is mandatory in group fetching", true)
        }
    }
    
    //Fetches all the groups from the DB
    func getGroups(completionHandler: @escaping([Group], Bool) -> Void) {
        let fetchRequest : NSFetchRequest<Group> = Group.fetchRequest()
        
        do {
            let result = try container.viewContext.fetch(fetchRequest)
            completionHandler(result, false)
        }
        catch {
            print("Error getting groups")
            completionHandler([], true)
        }
    }
    
}
